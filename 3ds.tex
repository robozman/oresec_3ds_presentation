\documentclass{oresec}

\usepackage{fontawesome}
\usepackage{etoolbox}
\usepackage{textcomp}
\usepackage[nodisplayskipstretch]{setspace}
\usepackage{xspace}
\usepackage{verbatim}
\usepackage{multicol}
\usepackage{soul}
\usepackage{attrib}
\usepackage[export]{adjustbox}

\usepackage{amsmath,amssymb,amsthm}

\usepackage[linesnumbered,commentsnumbered,ruled,vlined]{algorithm2e}
\newcommand\mycommfont[1]{\footnotesize\ttfamily\textcolor{blue}{#1}}
\SetCommentSty{mycommfont}
\SetKwComment{tcc}{ \# }{}
\SetKwComment{tcp}{ \# }{}

\usepackage{siunitx}

\usepackage{tikz}
\usepackage{pgfplots}
\usetikzlibrary{decorations.pathreplacing,calc,arrows.meta,shapes,graphs}

\AtBeginEnvironment{minted}{\singlespacing\fontsize{10}{10}\selectfont}
\usefonttheme{serif}

\makeatletter
\patchcmd{\beamer@sectionintoc}{\vskip1.5em}{\vskip0.5em}{}{}
\makeatother

% Math stuffs
\newcommand{\Z}{\mathbb{Z}}
\newcommand{\R}{\mathbb{R}}
\newcommand{\N}{\mathbb{N}}
\newcommand{\lcm}{\text{lcm}}
\newcommand{\Inn}{\text{Inn}}
\newcommand{\Aut}{\text{Aut}}
\newcommand{\Ker}{\text{Ker}\ }
\newcommand{\la}{\langle}
\newcommand{\ra}{\rangle}

\newcommand{\yournewcommand}[2]{Something #1, and #2}

\newenvironment{question}[1]{\par\textbf{Question #1.}\par}{}

\newcommand{\pmidg}[1]{\parbox{\widthof{#1}}{#1}}
\newcommand{\splitslide}[4]{
    \noindent
    \begin{minipage}{#1 \textwidth - #2 }
        #3
    \end{minipage}%
    \hspace{ \dimexpr #2 * 2 \relax }%
    \begin{minipage}{\textwidth - #1 \textwidth - #2 }
        #4
    \end{minipage}
}

\newcommand{\frameoutput}[1]{\frame{\colorbox{white}{#1}}}

\newcommand{\tikzmark}[1]{%
\tikz[baseline=-0.55ex,overlay,remember picture] \node[inner sep=0pt,] (#1)
{\vphantom{T}};
}

\newcommand{\braced}[3]{%
    \begin{tikzpicture}[overlay,remember picture]
        \draw [thick,decorate,decoration={brace,raise=1ex,amplitude=4pt},blue] (#2.south west-|T1.south west) -- node[anchor=west,left,xshift=-1.8ex,text=olive]{#3} (#1.north west-|T1.south west);
    \end{tikzpicture}
}

\newcommand{\make}{GNU \texttt{make}\xspace}

\title{Hacking the 3DS}
\author{Robby Zampino}
\institute{Mines Cybersecurity Club}

\begin{document}

\section{Overview}

\begin{frame}{Overview}
    1. Overview of the 3DS hardware

    \pause

    2. Overview of the 3DS security and OS
    
    \pause

    3. Overview of ROP and Buffer Overflows / Stack Smashing
    
    \pause

    4. Exploiting the 3DS
\end{frame}

\section{Motivations}

\begin{frame}{Motivations}
        \begin{itemize}
                \item Improve system performance
                        \begin{itemize}
                                \pause
                                \item Leverage New3ds improvements in old games
                        \end{itemize}
                \pause
                \item Modify the OS to add new features
                        \begin{itemize}
                                \pause
                                \item Input redirection
                                \pause
                                \item Video streaming
                        \end{itemize}
                \pause
                \item Enable game modding
                        \begin{itemize}
                                \pause
                                \item Majoras Mask Restoration
                                \pause
                                \item Translation patches
                        \end{itemize}
                \pause
                \item Cheats and save editors
                \pause
                \item Install homebrew
                        \begin{itemize}
                                \pause
                                \item Save game backup tools
                                \pause
                                \item Game backup tools
                                \pause
                                \item Emulators
                        \end{itemize}
                \pause
                \item Disable region locking
                        \begin{itemize}
                                \pause
                                \item Use foreign console in the US (or vice versa)
                                \pause
                                \item Import games that were only released in certain regions
                        \end{itemize}
        \end{itemize}
\end{frame}

\section{Definitions}

\begin{frame}{Hardware}
        \textbf{ARM}: A family of RISC architectures for computer processors. Commonly used in mobile applications (phones, portable game consoles, televisions, some chromebooks)

        \pause
        \textbf{CPU}: Graphics Processing Unit; Drives the graphics output of the 3DS

        \pause
        \textbf{SoC}: System on a Chip; An IC that integrated most or all of the components of a computer system (CPU, GPU, DSP, etc)
\end{frame}

\begin{frame}{Memory}
        \begin{columns}
                \column{.92\textwidth}
                     \pause
                     \textbf{Data Segment:}
                     \begin{itemize}
                             \pause  
                             \item Contains global variables and static variables which have a predefined value and can be modified
                             \pause
                             \item In other words: any variables not defined in a function or are declared static
                             \pause
                             \item Generally marked read-write and non-executable
                     \end{itemize}
                     \pause
                     \textbf{Text Segment:}
                     \begin{itemize}
                             \pause
                             \item Contains executable instructions for the program
                             \pause
                             \item Generally is marked read-only and executable
                     \end{itemize}
                     \pause
                 \column{.5\textwidth}
                 \includegraphics[width=.5\linewidth]{graphics/page1-467px-Program_memory_layout}
        \end{columns}
\end{frame}

\begin{frame}{Memory}
        \begin{columns}
                \column{.92\textwidth}
                     \pause
                     \textbf{Stack Frame:}
                     \begin{itemize}
                             \pause
                             \item Modeled after the stack data structure
                             \pause
                             \item Broken up into stack frames
                             \begin{itemize}
                                     \pause
                                     \item Each stack frame contains the variables for one function call
                                     \pause
                                     \item Stack frames consist at minimum of a return address
                             \end{itemize}
                     \end{itemize}
                 \column{.5\textwidth}
                 \includegraphics[width=.5\linewidth]{graphics/page1-467px-Program_memory_layout}
        \end{columns}
\end{frame}
\section{The 3DS Hardware}

\begin{frame}{3DS Models}
        \includegraphics[width=\linewidth]{graphics/console_version_table}
\end{frame}

\begin{frame}{Hardware Overview}
        \includegraphics[width=\linewidth]{graphics/diag-overview}
\end{frame}

\begin{frame}{ARM11 Overview}
        \hspace{-.6\linewidth}
        \includegraphics[width=2\linewidth, inner]{graphics/diag-arm11-0}
\end{frame}

\begin{frame}{ARM11 Overview}
        \hspace{-.6\linewidth}
        \includegraphics[width=2\linewidth, inner]{graphics/diag-arm11-1}
\end{frame}

\begin{frame}{ARM11 Overview}
        \hspace{-.6\linewidth}
        \includegraphics[width=2\linewidth, inner]{graphics/diag-arm11-2}
\end{frame}

\begin{frame}{ARM11 Overview}
        \hspace{-.6\linewidth}
        \includegraphics[width=2\linewidth, inner]{graphics/diag-arm11-3}
\end{frame}

\begin{frame}{Hardware Overview}
        \includegraphics[width=\linewidth]{graphics/diag-overview}
\end{frame}

\begin{frame}{ARM9 Overview}
        \hspace{-.1\linewidth}
        \includegraphics[width=1\linewidth, inner]{graphics/diag-arm9-0}
\end{frame}

\begin{frame}{ARM9 Overview}
        \hspace{-.1\linewidth}
        \includegraphics[width=1\linewidth, inner]{graphics/diag-arm9-0}
\end{frame}

\begin{frame}{ARM9 Overview}
        \hspace{-.1\linewidth}
        \includegraphics[width=1\linewidth, inner]{graphics/diag-arm9-1}
\end{frame}

\begin{frame}{ARM9 Overview}
        \hspace{-.1\linewidth}
        \includegraphics[width=1\linewidth, inner]{graphics/diag-arm9-2}
\end{frame}

\begin{frame}{ARM9 Overview}
        \hspace{-.1\linewidth}
        \includegraphics[width=1\linewidth, inner]{graphics/diag-arm9-3}
\end{frame}

\begin{frame}{ARM9 Overview}
        \hspace{-.1\linewidth}
        \includegraphics[width=1\linewidth, inner]{graphics/diag-arm9-4}
\end{frame}

\begin{frame}{Privledge Overview}
        \includegraphics[width=1\linewidth, inner]{graphics/3ds_priv_levels}
\end{frame}

\begin{frame}{Privledge Overview}
        \includegraphics[width=1\linewidth, inner]{graphics/3ds_priv_levels_fixed}
        Thanks to SVC Backdoor
\end{frame}

\section{Vulnerability Overview}

\begin{frame}{Buffer Overflows}
        \pause
        \textbf{Buffer Overflows:}
        \begin{itemize}
                \pause
                \item Vulnerability where a write to an array extends past the end
                \pause
                \item Found in languages that do not have explicit bounds checking (C and C++ for example)
        \end{itemize}
\end{frame}

\begin{frame}[fragile]{Buffer Overflows}
        \pause
        \textbf{Let's look at some C code}
        \begin{minted}{c}
        char A[8] = "";
        uint16 b = 1979;
        \end{minted}
        \pause
        \textbf{Current state of memory}
        \begin{tabular}{ l c r}
                variable name & A & B \\
                value & [nothing] & 1979 \\ 
                hex value & 00 00 00 00 00 00 00 00 & 07 BB \\ 
        \end{tabular}

        \pause
        \textbf{Let's copy some text into A}
        \begin{minted}{c}
        strcpy(A, "excessive");
        \end{minted}
        \pause
        \textbf{Current state of memory}
        \begin{tabular}{ l c r}
                variable name & A & B \\
                value & 'e' 'x' 'c' 'e' 's' 's' 'i' 'v' & \color{red}25856 \\ 
                hex value & 65 78 63 65 73 73 69 76 & \color{red}65 00 \\ 
        \end{tabular}
\end{frame}

\begin{frame}{Buffer Overflows}
        \pause
        \textbf{Why would we want to do this?}
        \begin{itemize}
                \pause
                \item Overwrite arbitrary locations in memory
                \pause
                \item Run our own custom code on the system
                \pause
                \item Corrupt system memory at will
        \end{itemize}
\end{frame}


\begin{frame}{Buffer Overflows}
        \pause
        \textbf{Protections against buffer overflows}
        \begin{itemize}
                \pause
                \item Memory is never mapped RWX or WX, only R, RW, or RX
                \pause
                \item This prevents us from writing arbitrary instructions for the system to execute
                \pause
                \item We still can overwrite the stack frames
                \pause
                \item The stack frames have a return address field
                \pause
                \item What do we do? Overwrite the stack and ROP
        \end{itemize}
\end{frame}

\begin{frame}[fragile]{Return Oriented Programming}
        \pause
        \textbf{Return Oriented Programming}
        \begin{itemize}
                \pause
                \item Cannot write our own code, but can overwrite the return address 
                \pause
                \item Find code already in memory (gadgets) that does what we need
                \pause
                \item Craft a fake stack that executes all the instructions we need
                \pause
                \begin{minted}{asm}
                .word ROP_POP_R0PC ; pop {r0, pc}
                .word dst ; r0
                .word ROP_POP_R1PC ; pop {r1, pc}
                .word src ; r1
                .word ROP_POP_R2R3R4R5R6PC ; pop {r2, r3, r4, r5, r6, pc}
                .word size ; r2
                .word 0xDEADBABE ; r3 (garbage)
                .word 0xDEADBABE ; r4 (garbage)
                .word 0xDEADBABE ; r5 (garbage)
                .word 0xDEADBABE ; r6 (garbage)
                .word MEMCPY
                \end{minted}
        \end{itemize}
\end{frame}

\section{Exploiting the 3DS}

\begin{frame}{oothax}
        \begin{columns}
                \column{.95\textwidth}
                    The Legend of Zelda: Ocarina of Time 3D
                    \begin{itemize}
                            \pause
                            \item Released June 2011
                            \pause
                            \item Has sold over 5 million copies
                            \pause
                            \item 94/100 on Metacritic
                            \pause
                            \item 0/100 on Codecritic
                    \end{itemize}
                \column{.5\textwidth}
                \includegraphics[width=.4\linewidth]{graphics/The_Legend_of_Zelda_Ocarina_of_Time_3D_box_art}
        \end{columns}
\end{frame}

\begin{frame}{oothax}
        \pause
        \textbf{Exploit}
        \begin{itemize}
              \pause
              \item OOT3D allows you to name your save file and character
              \pause
              \item This name is stored in the save file, along with a length value
              \pause
              \item What happens if you overwrite the name and length value?
        \end{itemize}
        \pause
        \textbf{Protections}
        \begin{itemize}
                \pause
                \item 3DS save files are stored on the SD card for eShop games and the game cart for physical games 
                \pause
                \item Modifying SD card save files is not possible
                \pause
                \item ARM9 Crypo CPU encrypts / signs save files with key that is locked away in hardware
                \pause
                \item How about game cart save files?
        \end{itemize}
\end{frame}

\begin{frame}{Powersaves}
        \includegraphics[width=.3\linewidth]{graphics/powersaves_box}
        \includegraphics[width=.3\linewidth]{graphics/powersaves_cart}

        \pause
        \textbf{Powersaves}
        \begin{itemize}
                \pause
                \item Hardware accessory that can modify saves and apply cheats to 3DS games over USB from a Windows PC
                \pause
                \item Can overwrite game cart save files
        \end{itemize}
        \pause
        What happens if we create a carefully written save file?
\end{frame}

\begin{frame}{oothax}
    https://www.youtube.com/watch?v=xWopLJaAsCc
\end{frame}

\begin{frame}{Fin}
        This talk only covered the surface of 3DS hacking.

        There are many other ARM11 userland exploits (Webkit, ninjahax, ...).

        There is also the ARM11 kernel and ARM9 cpus to hack.

        This does not even give you access to the crypto keys.

\end{frame}

\begin{frame}{Demo time?}
        Demo time?

        Questions?
\end{frame}

\end{document}
% Local Variables:
% TeX-command-extra-options: "-shell-escape"
% End:
